(function ($, Pixastic) {
  "use strict";

  var canvas;
  var image = new Image();
  image.src = "frank.jpg";

  var contrastSlider    = $( "#contrast" );
  var brightnessSlider  = $( "#brightness" );
  var saturationSlider  = $( "#saturation" );

  function initSliders() {
    contrastSlider.slider({
      value: 0,
      min: -100,
      max: 100,
    });
    brightnessSlider.slider({
      value: 0,
      min: -100,
      max: 100,
    });
    saturationSlider.slider({
      value: 0,
      min: -100,
      max: 100,
    });
  }

  function initCanvas() {
    canvas = document.getElementById('image-canvas');
    var context = canvas.getContext('2d');
    canvas.width = image.width;
    canvas.height = image.height; 
    context.width = image.width;
    context.height = image.height;
    context.drawImage(image, 0, 0, image.width, image.height);
  }

  function initComponents() {
    initCanvas();
    initSliders();
  }

  function initHandlers() {
    contrastSlider.on('slidechange', function(event, ui) {
      onSlidestopHandler();
    });
    brightnessSlider.on('slidechange', function(event, ui) {
      onSlidestopHandler();
    });
    saturationSlider.on('slidechange', function(event, ui) {
      onSlidestopHandler();
    });
  }

  function onSlidestopHandler() {
    
    var contrastValue    = (contrastSlider.slider('option', 'value')/100);
    var brightnessValue  = brightnessSlider.slider('option', 'value');
    var saturationValue  = saturationSlider.slider('option', 'value');

    Pixastic.process(
      canvas, 
      "brightness", 
      {
        brightness:brightnessValue,
        contrast:contrastValue,
      },
      function(canvas) {
        Pixastic.process(
          canvas, 
          "hsl", 
          {
            saturation:saturationValue,
          },
          function(canvas) {
            $('.image-pane').html(canvas);
          }
        );
      }
    );

  }

  function init() {
    initComponents();
    initHandlers();
  }

  image.onload = function() {
    init();
  };
  
})(jQuery, Pixastic);